
package xmlwrite1;

import java.io.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

public class XMLwrite1 {

    public static void main(String[] args) {
       
        File file = new File("C:/Users/Shion/Documents/NetBeansProjects/XMLwrite1/src/xmlwrite1/quotes.xml");
       
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuild = factory.newDocumentBuilder();
            Document doc = (Document) docBuild.newDocument();
            
            Element rootE = doc.createElement("quotes");
            doc.appendChild(rootE);
            
            Element quoteE1 = doc.createElement("quote");
            rootE.appendChild(quoteE1);
            
            Element wordE1 = doc.createElement("word");
            wordE1.appendChild(doc.createTextNode("Timee is more value than money. You can get more money, "
                    + "but you cannot get more time"));
            quoteE1.appendChild(wordE1);
            
            Element byE1 = doc.createElement("by");
            byE1.appendChild(doc.createTextNode("Jim Rohn"));
            quoteE1.appendChild(byE1);
            
            Element quoteE2 = doc.createElement("quote");
            rootE.appendChild(quoteE2);

            Element wordE2 = doc.createElement("word");
            wordE2.appendChild(doc.createTextNode("เมื่อทำอะไรสำเร็จ แม้จะเป็นเก้าเล้กๆของตัวเอง "
                    + "ก็ควรรู้จักให้รางวัลตัวเองบ้าง"));
            quoteE2.appendChild(wordE2);

            Element by2E = doc.createElement("by");
            by2E.appendChild(doc.createTextNode("ว. วชิรเมธี"));
            quoteE2.appendChild(by2E);

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(file);

            trans.transform(source, result);

            System.out.println("Create quotes.xml Success!!!");
            
        } catch (Exception ex) {
            System.out.println(ex);
        }
    
    }
}
