package xmlsearcher1;

import java.io.*;
import java.util.Scanner;
import javax.xml.parsers.*;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLSearcher1 {

    public static void main(String[] args) throws FileNotFoundException, 
            ParserConfigurationException, SAXException, IOException {
        
        String keywordFile = "C:/Users/Shion/Documents/NetBeansProjects/XMLSearcher1/src/xmlsearcher1/keyword.txt";
        File xmlFile = new File("C:/Users/Shion/Documents/NetBeansProjects/XMLSearcher1/src/xmlsearcher1/quotes.xml");
        Scanner scan = new Scanner(new FileInputStream(keywordFile));
        try  {
            while(scan.hasNext()){
                keywordFile = scan.nextLine();
                DocumentBuilderFactory factory  = DocumentBuilderFactory.newInstance();
                DocumentBuilder docbuild = factory.newDocumentBuilder();
                Document doc = docbuild.parse(xmlFile);
                               
                
                 NodeList GEquotes = doc.getElementsByTagName("quote");
            for (int i = 0; i < GEquotes.getLength(); i++) {
                    Element item = (Element) GEquotes.item(i);

                    if (getElemVal(item, "by").toLowerCase().contains(keywordFile.toLowerCase())) {
                        System.out.println(getElemVal(item, "word") + " by " + getElemVal(item, "by"));
                    }
                }
            }
        } finally {
            scan.close();
        }
    }

    protected static String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        } catch (Exception ex) {
        }
        return "";
    }
    
}

