package lab;

import java.io.*;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;

@WebServlet(name = "XMLSearch2", urlPatterns = {"/XMLSearch2"})
public class XMLSearch2 extends HttpServlet {
    private XMLEventReader reader;


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, XMLStreamException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        String URLInput = request.getParameter("url");
        String keywordInput = request.getParameter("key");
        boolean titleInput = false;
        boolean linkInput = false;
        boolean itemInput = false;
        String title = null;
        String link = null;
        String eName = null;
        
        try {
            URL ul = new URL(URLInput);
            InputStream ins = ul.openStream();
            XMLInputFactory factory = XMLInputFactory.newInstance();
            
            reader = factory.createXMLEventReader(ins);
        out.print(
                "<html><body><table border = '1'><tr ><th>Title</th>"
                + "<th>Link</th ></tr>");
        while (reader.hasNext()){
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()){
                StartElement element = (StartElement) event;
                eName = element.getName().getLocalPart();
            
                 if (eName.equals("item")){
                    itemInput = true;
                }
                 if (itemInput && eName.equals("link")){
                    linkInput = true;
                }
                if (itemInput && eName.equals("title")){
                    titleInput = true;
                }        
                }
            if (event.isEndElement()){
                EndElement element = (EndElement) event;
                eName = element.getName().getLocalPart();
                if (eName.equals("item")){
                    itemInput = false;
                }
                 if (itemInput && eName.equals("link")){
                    linkInput = false;
                }
                if (itemInput && eName.equals("title")){
                    titleInput = false;
                }    
            }
            if (event.isCharacters()){
                Characters character = (Characters) event;
                
                if (titleInput){
                    title = character.getData();
                }
                if (linkInput){
                    link = character.getData();
                    
                    if(title.toLowerCase().contains(keywordInput.toLowerCase())){
                        out.print("<tr><td>" + title + "</td>");
                        out.print("<td><a href='" + link + "'>" + link + "</a></td></tr>");
                             
                    }
                }
            }
        }
         
        reader.close();
        out.print("</table></body></html>");
    } catch (Exception ex){
            ex.printStackTrace(System.err);        
    } finally {
            out.close();
        }
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (XMLStreamException ex) {
            Logger.getLogger(XMLSearch2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (XMLStreamException ex) {
            Logger.getLogger(XMLSearch2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
