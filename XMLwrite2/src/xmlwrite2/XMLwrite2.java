package xmlwrite2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class XMLwrite2 {

    public static void main(String[] args) {
        
        File file = new File("C:/Users/Shion/Documents/NetBeansProjects/XMLwrite2/src/xmlwrite2/quotes.xml");

        try {

            XMLOutputFactory xmlOF = XMLOutputFactory.newInstance();
            XMLStreamWriter xmlSW = xmlOF.createXMLStreamWriter(new OutputStreamWriter(
                    new FileOutputStream(file)));

            xmlSW.writeStartDocument("UTF-8", "1.0");

            xmlSW.writeStartElement("quotes");

            xmlSW.writeStartElement("quote");
            xmlSW.writeStartElement("word");
            xmlSW.writeCharacters("Time is more value than money. You can get more money, but you cannot get more time");
            xmlSW.writeEndElement();
            xmlSW.writeStartElement("by");
            xmlSW.writeCharacters("Jim Rohn");
            xmlSW.writeEndElement();
            xmlSW.writeEndElement();

            xmlSW.writeStartElement("quote");
            xmlSW.writeStartElement("word");
            xmlSW.writeCharacters("เมื่อทำอะไรสำเร็จ แม้จะเป็นเก้าเล้กๆของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
            xmlSW.writeEndElement();
            xmlSW.writeStartElement("by");
            xmlSW.writeCharacters("ว. วชิรเมธี");
            xmlSW.writeEndElement();
            xmlSW.writeEndElement();

            xmlSW.writeEndDocument();
            xmlSW.flush();
            xmlSW.close();
            
            System.out.println("Create quotes.xml Success!!!");
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
   }

