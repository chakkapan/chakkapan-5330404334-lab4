package lab;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.stream.*;
import javax.xml.stream.events.*;

public class FeedWrite2 extends HttpServlet {

    String filePath = "C:/Users/Shion/Documents/NetBeansProjects/WebApplication3/src/java/lab/feed2.xml";
    File file = new File(filePath);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        try {
            String rssTitle = request.getParameter("title");
            String rssLink = request.getParameter("link");
            String rssDescription = request.getParameter("description");

            if (file.exists()) {
                new FeedWrite2().updateRssFeed(rssTitle, rssDescription, rssLink);
            } else {
                new FeedWrite2().createRssFeed(rssTitle, rssDescription, rssLink);
            }

            out.println("<b <a href='http://localhost:8080/WebApplication3/FeedWrite2.xml'>" 
                    + "Rss Feed</a> was create successfully</b>");

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    private void createRssFeed(String rssTitle, String rssDescription, String rssUrl) 
            throws Exception {
    
        XMLOutputFactory xmlOF = XMLOutputFactory.newInstance();
      
        XMLStreamWriter xmlSW = xmlOF.createXMLStreamWriter(new OutputStreamWriter(
                new FileOutputStream(file)));

        xmlSW.writeStartDocument();
        xmlSW.writeStartElement("rss");
        xmlSW.writeAttribute("version", "2.0");
        xmlSW.writeStartElement("channel");
        xmlSW.writeStartElement("title");
        xmlSW.writeCharacters("Khon Kean University RSS Feed");
        xmlSW.writeEndElement();
      
        xmlSW.writeStartElement("description");
        xmlSW.writeCharacters("Khon Kean University Information News RSS Feed");
        xmlSW.writeEndElement();
  
        xmlSW.writeStartElement("link");
        xmlSW.writeCharacters("http://www.kku.ac.th");
        xmlSW.writeEndElement();
       
        xmlSW.writeStartElement("lang");
        xmlSW.writeCharacters("en~th");
        xmlSW.writeEndElement();
   
        xmlSW.writeStartElement("item");
  
        xmlSW.writeStartElement("title");
        xmlSW.writeCharacters(rssTitle);
        xmlSW.writeEndElement();
       
        xmlSW.writeStartElement("description");
        xmlSW.writeCharacters(rssDescription);
        xmlSW.writeEndElement();
        
        xmlSW.writeStartElement("link");
        xmlSW.writeCharacters(rssUrl);
        xmlSW.writeEndElement();
        
        xmlSW.writeStartElement("pubDate");
        xmlSW.writeCharacters((new java.util.Date()).toString());
        xmlSW.writeEndElement();
        xmlSW.writeEndElement();
     
        xmlSW.writeEndElement();
   
        xmlSW.writeEndElement();
        
        xmlSW.writeEndDocument();
        xmlSW.flush();
        xmlSW.close();
    }

    private void updateRssFeed(String rssTitle, String rssDescription, String rssUrl) throws Exception {
       
        XMLInputFactory xmlIF = XMLInputFactory.newInstance();
        
        XMLEventReader feedReader = xmlIF.createXMLEventReader(new InputStreamReader(new FileInputStream(file)));
        
        XMLOutputFactory xmlOF = XMLOutputFactory.newInstance();
        
        XMLStreamWriter feedWriter = xmlOF.createXMLStreamWriter(new OutputStreamWriter(new FileOutputStream(file)));
        String elementName;

        while (feedReader.hasNext()) {
            XMLEvent event = feedReader.nextEvent();

            if (event.getEventType() == XMLEvent.START_DOCUMENT) {
          
                feedWriter.writeStartDocument();
            }

            if (event.getEventType() == XMLEvent.START_ELEMENT) {
                StartElement element = (StartElement) event;
                elementName = element.getName().getLocalPart();
                if (elementName.equals("rss")) {
                    feedWriter.writeStartElement("rss");
                    feedWriter.writeAttribute("version", "2.0");
                } else {
                    feedWriter.writeStartElement(elementName);
                }
            }

            if (event.isEndElement()) {
                EndElement element = (EndElement) event;
                elementName = element.getName().getLocalPart();
                if (elementName.equals("channel")) {
              
                    feedWriter.writeStartElement("item");
           
                    feedWriter.writeStartElement("title");
                    feedWriter.writeCharacters(rssTitle);
                    feedWriter.writeEndElement();
   
                    feedWriter.writeStartElement("description");
                    feedWriter.writeCharacters(rssDescription);
                    feedWriter.writeEndElement();
               
                    feedWriter.writeStartElement("link");
                    feedWriter.writeCharacters(rssUrl);
                    feedWriter.writeEndElement();
                 
                    feedWriter.writeStartElement("pubDate");
                    feedWriter.writeCharacters((new java.util.Date()).toString());
                    feedWriter.writeEndElement();
                    feedWriter.writeEndElement();
                    feedWriter.writeEndDocument();
                    feedWriter.flush();
                    feedWriter.close();
                    return;
                } else {
                    feedWriter.writeEndElement();
                }
            }
            
            if (event.isCharacters()) {
                Characters characters = (Characters) event;
                feedWriter.writeCharacters(characters.getData().toString());
            }
        }

        feedWriter.flush();
        feedWriter.close();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}