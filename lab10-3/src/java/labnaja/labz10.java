package labnaja;
   
import javax.xml.soap.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;


public class labz10 {

    public static void main(String[] args) throws Exception {
        String pinId = "";

        labz10 ServicePIN = new labz10();
        ServicePIN.processRequest(pinId);
    }

    public void processRequest(String pinId) throws Exception {

        SOAPConnectionFactory soapCon =
                SOAPConnectionFactory.newInstance();
        SOAPConnection connection =
                soapCon.createConnection();
        MessageFactory mFac =
                MessageFactory.newInstance();

        SOAPMessage message =
                mFac.createMessage();

        SOAPHeader header = message.getSOAPHeader();
        SOAPBody body = message.getSOAPBody();
        header.detachNode();

        SOAPFactory soapFactory =
                SOAPFactory.newInstance();
        SOAPBodyElement ServicePIN =
                body.addBodyElement(soapFactory.createName("ServicePIN",
                "ns", "https://rdws.rd.go.th/ServiceRD/CheckTINPINService"));

        SOAPElement username =
                ServicePIN.addChildElement(
                soapFactory.createName("username", "ns",
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService"));
        username.addTextNode("anonymous");

        SOAPElement password =
                ServicePIN.addChildElement(
                soapFactory.createName("password", "ns",
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService"));
        password.addTextNode("anonymous");

        SOAPElement pinE =
                ServicePIN.addChildElement(
                soapFactory.createName("PIN", "ns",
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService"));

        // Add pin to pin node
        pinE.addTextNode(pinId);

        MimeHeaders h = message.getMimeHeaders();
        h.addHeader("SOAPAction",
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService/ServicePIN");

        message.saveChanges();
        System.out.println("REQUEST:");
        displayMessage(message);

        System.out.println("\n");
        labx10.install();
        SOAPConnection conn =
                SOAPConnectionFactory.newInstance().createConnection();
        SOAPMessage response = conn.call(message,
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService.asmx");

        System.out.println("RESPONSE:");

        displayMessage(response);
    }

    private void displayMessage(SOAPMessage message) {
        try {
            TransformerFactory tFac = TransformerFactory.newInstance();
            Transformer transformer = tFac.newTransformer();
            Source src = message.getSOAPPart().getContent();
            StreamResult result = new StreamResult(System.out);
            transformer.transform(src, result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
