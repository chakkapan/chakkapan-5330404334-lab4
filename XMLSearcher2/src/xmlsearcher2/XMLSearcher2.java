package xmlsearcher2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Scanner;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class XMLSearcher2 {

    public static void main(String argv[]) throws FileNotFoundException, XMLStreamException
             {
        String keywordFilePath = "keyword.txt";
        File quotesFile = new File("C:/Users/Shion/Documents/NetBeansProjects/XMLSearcher2/quote.xml");
        boolean quoteFound = false;
        boolean wordFound = false;
        boolean byFound = false;
        String keyword = null;
        String word = null;
        String by = null;
        String eName = null;
        Scanner scan = new Scanner(new FileInputStream(keywordFilePath), "UTF-8");

          try {
            while (scan.hasNext()) {
                keyword = scan.next();
               
                XMLInputFactory factory = XMLInputFactory.newInstance();
                XMLEventReader reader = factory.createXMLEventReader(new FileInputStream(quotesFile));

                
                while (reader.hasNext()) {
                    
                    XMLEvent event = reader.nextEvent();
                    if (event.isStartElement()) {
                        StartElement element = (StartElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                           quoteFound = true;
                        }
                        if (quoteFound && eName.equals("word")) {
                            wordFound = true;
                        }
                        if (quoteFound && eName.equals("by")) {
                            byFound = true;
                        }
                    }

                    if (event.isEndElement()) {
                        EndElement element = (EndElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            quoteFound = false;
                        }
                        if (quoteFound && eName.equals("word")) {
                            wordFound = false;
                        }
                        if (quoteFound && eName.equals("by")) {
                            byFound = false;
                        }
                    }

                    if (event.isCharacters()) {
                        Characters characters = (Characters) event;

                        if(byFound) {
                            by = characters.getData();
                            
                            if(by.toLowerCase().contains(keyword.toLowerCase())) {
                             
                                System.out.println(word + " by " + by);
                            }
                        }
                        if(wordFound) {
                            word = characters.getData();
                        }
                    }
                }
            }
        } finally {
            scan.close();
        }            
    }    
}
