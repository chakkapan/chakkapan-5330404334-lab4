package lab;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

public class FeedWrite1 extends HttpServlet {
    
    Document doc;
    String filePath = "C:/Users/Shion/Documents/NetBeansProjects/WebApplication2/src/java/lab/feed.xml";
    File file = new File(filePath);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
  
        String rssTitle = request.getParameter("title");
        String rssLink = request.getParameter("link");
        String rssDescrip = request.getParameter("description");
        
        try {
             if (file.exists()) {
                DocumentBuilderFactory builderFa = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = builderFa.newDocumentBuilder();
                doc = (Document) docBuilder.parse(file);
                FeedWrite1 feedW1 = new FeedWrite1();
                String feed = feedW1.updateRss(doc, rssTitle, rssLink, rssDescrip);
                out.println(feed);
            } else {
                
                DocumentBuilderFactory builderFa = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = builderFa.newDocumentBuilder();
                doc = (Document) docBuilder.newDocument();
                FeedWrite1 feedW1 = new FeedWrite1();
                String feed = feedW1.createRss(doc, rssTitle, rssLink, rssDescrip);
                out.print(feed);
                         
            } 
        }catch (Exception ex){
            System.out.println(ex);
            
        }
    }

     public String createRss(Document doc, String rssTitle, String rssUrl, String rssDescription) 
             throws Exception {
    
        Element rss = doc.createElement("rss");
           
        rss.setAttribute("version", "2.0");
        doc.appendChild(rss);
        
        Element channel = doc.createElement("channel");
        rss.appendChild(channel);

        Element title = doc.createElement("title");
        channel.appendChild(title);
        Text titleTex = doc.createTextNode("Khon Kean University Rss Feed");
        title.appendChild(titleTex);
        
        Element desc = doc.createElement("description");
        channel.appendChild(desc);
        Text descTex = doc.createTextNode("Khon Kea University Information News Rss Feed");
        desc.appendChild(descTex);

        Element link = doc.createElement("link");
        channel.appendChild(link);
        Text linkT = doc.createTextNode("http://www.kku.ac.th");
        link.appendChild(linkT);

        Element lang = doc.createElement("lang");
        channel.appendChild(lang);
        Text langT = doc.createTextNode("en-th");
        lang.appendChild(langT);

        Element item = doc.createElement("item");
        channel.appendChild(item);

        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleTex = doc.createTextNode(rssTitle);
        iTitle.appendChild(iTitleTex);
        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(rssDescription);
        iDesc.appendChild(iDescT);
        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(rssUrl);
        iLink.appendChild(iLinkT);
        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);

        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        return xmlString;
    }

    public String updateRss(Document doc, String rssTitle, String rssUrl, String rssDescription) throws Exception {
        Element channel = (Element) doc.getElementsByTagName("channel").item(0);

        Element item = doc.createElement("item");
        channel.appendChild(item);
        
        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(rssTitle);
        iTitle.appendChild(iTitleT);

        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(rssDescription);
        iDesc.appendChild(iDescT);

        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(rssUrl);
        iLink.appendChild(iLinkT);

        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);

        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        StringWriter StrWri = new StringWriter();
        StreamResult result = new StreamResult(StrWri);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = StrWri.toString();

        BufferedWriter BuffW = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        BuffW.write(xmlString);
        BuffW.flush();
        return xmlString;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short   description";
    }
}

      
